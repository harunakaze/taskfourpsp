package com.example.service;

import com.example.dao.CourseMapper;
import com.example.model.CourseModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by harunakaze on 11-Mar-17.
 */
@Slf4j
@Service
public class CourseServiceDatabase implements CourseService {

    @Autowired
    private CourseMapper courseMapper;

    @Override
    public CourseModel selectCourse(String idCourse) {
        log.info ("select course with idCourse {}", idCourse);
        return courseMapper.selectCourse(idCourse);
    }

    @Override
    public List<CourseModel> selectAllCourses() {
        log.info("select all course");
        return courseMapper.selectAllCourses();
    }

    @Override
    public void addCourse(CourseModel course) {
        log.info("add course with id {}", course.getIdCourse());
        courseMapper.addCourse(course);
    }

    @Override
    public void deleteCourse(String idCourse) {
        log.info("delete course with id {}", idCourse);
        courseMapper.deleteCourse(idCourse);
    }

    @Override
    public void updateCourse(CourseModel course) {
        log.info("update course with id {}", course.getIdCourse());
        courseMapper.updateCourse(course);
    }
}
